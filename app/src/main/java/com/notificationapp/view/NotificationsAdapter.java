package com.notificationapp.view;

import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.notificationapp.R;
import com.notificationapp.db.AppNotification;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationsAdapter extends PagedListAdapter<AppNotification, NotificationsAdapter.NotificationHolder> {

    protected NotificationsAdapter() {
        super(DIFF_CALLBACK);
    }

    @NonNull
    @Override
    public NotificationHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new NotificationHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationHolder holder, int position) {
        AppNotification notification = getItem(position);
        if (notification != null) {
            holder.bindTo(notification);
        }
    }

    static class NotificationHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ivAppIcon)
        ImageView ivAppIcon;
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvText)
        TextView tvText;
        @BindView(R.id.tvTime)
        TextView tvTime;
        @BindView(R.id.tvDate)
        TextView tvDate;

        public NotificationHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bindTo(AppNotification notification) {
            try {
                Drawable icon = itemView.getContext().getPackageManager().getApplicationIcon(notification.getAppPackage());
                ivAppIcon.setImageDrawable(icon);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
                ivAppIcon.setImageResource(R.mipmap.ic_launcher_round);
            }
            tvTitle.setText(notification.getTitle());
            tvText.setText(notification.getText());
            Date date = new Date(notification.getDateTime());
            tvTime.setText(DateFormat.getTimeInstance(DateFormat.SHORT).format(date));
            tvDate.setText(new SimpleDateFormat("dd.MM.yyyy").format(date));
        }
    }

    private static DiffUtil.ItemCallback<AppNotification> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<AppNotification>() {
                @Override
                public boolean areItemsTheSame(@NonNull AppNotification oldItem, @NonNull AppNotification newItem) {
                    return oldItem.getID() == newItem.getID();
                }

                @Override
                public boolean areContentsTheSame(@NonNull AppNotification oldItem, @NonNull AppNotification newItem) {
                    return oldItem.equals(newItem);
                }
            };
}
