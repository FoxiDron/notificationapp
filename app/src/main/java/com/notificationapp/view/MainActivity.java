package com.notificationapp.view;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.icu.util.TimeUnit;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.notificationapp.R;
import com.notificationapp.services.NotificationsCountReceiver;
import com.notificationapp.services.NotificationListener;
import com.notificationapp.viewModel.NotificationsViewModel;
import com.notificationapp.viewModel.ViewModelFactory;

import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;

public class MainActivity extends AppCompatActivity implements HasAndroidInjector {

    @Inject
    DispatchingAndroidInjector<Object> dispatchingAndroidInjector;

    @Override
    public AndroidInjector<Object> androidInjector() {
        return dispatchingAndroidInjector;
    }

    @Inject
    ViewModelFactory viewModelFactory;
    private NotificationsViewModel viewModel;
    private NotificationsAdapter adapter;

    @BindView(R.id.btnService)
    Button btnService;
    @BindView(R.id.rvNotifications)
    RecyclerView rvNotifications;
    @BindView(R.id.rlNoNotifications)
    RelativeLayout rlNoNotifications;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        updateBtn(isServiceRunning());
        startAlarmReceiver();

        btnService.setOnClickListener(v -> {
            if (isServiceRunning()) {
                Intent intent = new Intent(this, NotificationListener.class);
                intent.setAction("StopService");
                ContextCompat.startForegroundService(this, intent);
                updateBtn(false);
            } else {
                if (isNotificationSettingsGranted()) {
                    Intent intent = new Intent(this, NotificationListener.class);
                    intent.setAction("StartService");
                    ContextCompat.startForegroundService(this, intent);
                    updateBtn(true);
                }
            }
        });
        adapter = new NotificationsAdapter();
        rvNotifications.setLayoutManager(new LinearLayoutManager(this));
        rvNotifications.setAdapter(adapter);

        viewModel = new ViewModelProvider(this, viewModelFactory).get(NotificationsViewModel.class);
        viewModel.getGetAllNotifications().observe(this, appNotifications -> {
            if (appNotifications != null && appNotifications.size() > 0) {
                adapter.submitList(appNotifications);
                rlNoNotifications.setVisibility(View.GONE);
                rvNotifications.setVisibility(View.VISIBLE);
            } else {
                rlNoNotifications.setVisibility(View.VISIBLE);
                rvNotifications.setVisibility(View.GONE);
            }
        });
    }

    private void updateBtn(boolean running) {
        if (running) {
            btnService.setText("Stop");
            btnService.setTextColor(Color.BLACK);
            btnService.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.btnStopColor));
        } else {
            btnService.setText("Start");
            btnService.setTextColor(Color.WHITE);
            btnService.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.colorAccent));
        }
    }

    private boolean isServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (NotificationListener.class.getName().equals(service.service.getClassName())) {
                return service.started;
            }
        }
        return false;
    }

    private boolean isNotificationSettingsGranted() {
        final boolean enabled = NotificationManagerCompat.getEnabledListenerPackages(this).contains(getPackageName());
        if (!enabled) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Grant settings access");
            builder.setMessage("To listen incoming notifications you need grant access");
            builder.setNegativeButton("Cancel", null);
            builder.setPositiveButton("Grant", ((dialog, which) -> startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"))));
            builder.create().show();
            return false;
        } else return true;
    }

    private void startAlarmReceiver() {
        AlarmManager alarmMgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, NotificationsCountReceiver.class);
        PendingIntent alarmIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR, 4);
        alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), 1000 * 60 * 60 * 4, alarmIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.filter_all:
                viewModel.updateStartDate(Long.MIN_VALUE);
                if (item.isChecked()) item.setChecked(false);
                else item.setChecked(true);
                return true;
            case R.id.filter_per_hour:
                return endSwitch(item, Calendar.HOUR);
            case R.id.filter_per_day:
                return endSwitch(item, Calendar.DAY_OF_YEAR);
            case R.id.filter_per_month:
                return endSwitch(item, Calendar.MONTH);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private boolean endSwitch(MenuItem item, int calType) {
        Calendar cal = Calendar.getInstance();
        cal.add(calType, -1);
        viewModel.updateStartDate(cal.getTimeInMillis());
        if (item.isChecked()) item.setChecked(false);
        else item.setChecked(true);
        return true;
    }
}