package com.notificationapp.db;

import androidx.paging.DataSource;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

@Dao
public interface NotificationsDao {

    @Query("SELECT * FROM AppNotification WHERE date_time > :date ORDER BY date_time DESC")
    DataSource.Factory<Integer, AppNotification> getAll(long date);

    @Query("SELECT COUNT(*) FROM AppNotification")
    Integer getCount();

    @Insert
    void insert(AppNotification appNotification);
}
