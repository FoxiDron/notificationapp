package com.notificationapp.db;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Objects;

@Entity
public class AppNotification {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private long ID;
    @ColumnInfo(name = "app_package")
    private String appPackage;
    @ColumnInfo(name = "title")
    private String title;
    @ColumnInfo(name = "text")
    private String text;
    @ColumnInfo(name = "date_time")
    private long dateTime;

    public AppNotification(String appPackage, String title, String text, long dateTime) {
        this.appPackage = appPackage;
        this.title = title;
        this.text = text;
        this.dateTime = dateTime;
    }

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public String getAppPackage() {
        return appPackage;
    }

    public void setAppPackage(String appPackage) {
        this.appPackage = appPackage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getDateTime() {
        return dateTime;
    }

    public void setDateTime(long dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AppNotification that = (AppNotification) o;
        return ID == that.ID &&
                dateTime == that.dateTime &&
                appPackage.equals(that.appPackage) &&
                Objects.equals(title, that.title) &&
                Objects.equals(text, that.text);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ID, appPackage, title, text, dateTime);
    }
}
