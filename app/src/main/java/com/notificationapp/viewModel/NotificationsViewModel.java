package com.notificationapp.viewModel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

import com.notificationapp.db.AppNotification;
import com.notificationapp.repo.NotificationsRepo;

import javax.inject.Inject;

public class NotificationsViewModel extends ViewModel {

    private LiveData<PagedList<AppNotification>> getAllNotifications;
    private MutableLiveData<Long> startDate;

    @Inject
    public NotificationsViewModel(NotificationsRepo repo) {
        startDate = new MutableLiveData(Long.MIN_VALUE);
        getAllNotifications = Transformations.switchMap(startDate, date -> new LivePagedListBuilder<>(repo.getAllNotifications(date), 30).build());
    }

    public LiveData<PagedList<AppNotification>> getGetAllNotifications() {
        return getAllNotifications;
    }

    public void updateStartDate(long date) {
        startDate.postValue(date);
    }
}
