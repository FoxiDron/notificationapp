package com.notificationapp.repo;

import androidx.paging.DataSource;

import com.notificationapp.db.AppNotification;
import com.notificationapp.db.NotificationsDao;

import javax.inject.Inject;

public class NotificationsRepo {

    private NotificationsDao dao;

    @Inject
    public NotificationsRepo(NotificationsDao dao) {
        this.dao = dao;
    }

    public void insertNotification(AppNotification notification) {
        dao.insert(notification);
    }

    public int getNotificationsCount() {
        return dao.getCount();
    }

    public DataSource.Factory<Integer, AppNotification> getAllNotifications(long date) {
        return dao.getAll(date);
    }
}
