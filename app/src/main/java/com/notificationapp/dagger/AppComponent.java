package com.notificationapp.dagger;

import android.content.Context;

import com.notificationapp.MainApplication;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

@Singleton
@Component(modules = {CommonModule.class, ViewModelModule.class})
public interface AppComponent {

    void inject(MainApplication mainApplication);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder context(Context context);

        AppComponent build();
    }
}
