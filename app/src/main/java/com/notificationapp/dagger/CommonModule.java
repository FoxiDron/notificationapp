package com.notificationapp.dagger;

import android.content.Context;

import com.notificationapp.services.NotificationsCountReceiver;
import com.notificationapp.view.MainActivity;
import com.notificationapp.services.NotificationListener;
import com.notificationapp.db.AppDatabase;
import com.notificationapp.db.NotificationsDao;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Module(includes = {AndroidSupportInjectionModule.class})
public interface CommonModule {

    @Provides
    static AppDatabase provideAppDatabase(Context context) {
        return AppDatabase.getDatabase(context);
    }

    @Singleton
    @Provides
    static NotificationsDao provideNotificationDao(AppDatabase db) {
        return db.notificationsDao();
    }

    @ContributesAndroidInjector
    MainActivity mainActivityInjector();

    @ContributesAndroidInjector
    NotificationListener notificationListenerInjector();

    @ContributesAndroidInjector
    NotificationsCountReceiver notificationsCountReceiverInjector();
}
